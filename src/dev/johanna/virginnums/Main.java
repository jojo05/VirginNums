/*
    Written by Johanna on 12.1.2022. The code is public domain in it's entirety - CC0.
    Do with the code what you want.
    The code aims to find so-called "virgin numbers". Virgin numbers are considered numbers
    not used in any context anywhere, truly virgin, I guess. It's just a fun thing I thought
    about coding. My code sucks and Java sucks. Have a nice day.
 */
package dev.johanna.virginnums;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.io.BufferedInputStream;
public class Main {
    public static void main(String[] args) throws IOException {
        Requester requester = new Requester();
        requester.recurse();
    }
}